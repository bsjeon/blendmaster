varying lowp vec4 DestinationColor; // 1

varying lowp vec2 TexCoordOut;

uniform sampler2D Texture0;
uniform sampler2D Texture1;
//uniform float BlendFactor;

void main(void) { // 2
    vec4 texel0 = texture2D(Texture0, TexCoordOut);
    vec4 texel1 = texture2D(Texture1, TexCoordOut);
    gl_FragColor = texel1;// 3
}