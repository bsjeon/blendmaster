#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;


//const Vertex Vertices[] = {
//    {{1, -1, 0}, {1, 0, 0, 1}},
//    {{1, 1, 0}, {0, 1, 0, 1}},
//    {{-1, 1, 0}, {0, 0, 1, 1}},
//    {{-1, -1, 0}, {0, 0, 0, 1}}
//};

const float vertices [] = {0.8, -0.8, 0.8, 0.8, -0.8, 0.8, -0.8, -0.8};
const float colors [] = {1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1};
const float texCoords [] = {1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0};

const GLubyte Indices[] = {
    0, 1, 2,
    2, 3, 0
};


HelloWorld::HelloWorld()
:m_terrainTex(0),
m_highLightTex(0),
m_maskSprite(NULL),
m_mySprite(NULL)
{
    
}

HelloWorld::~HelloWorld()
{
    
}

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)))
    {
        return false;
    }


    string v_fileP = CCFileUtils::sharedFileUtils()->fullPathForFilename("multTex_v.vsh");
    CCString *v_source = CCString::createWithContentsOfFile(v_fileP.c_str());
    const GLchar *vSource = v_source->getCString();
    
    string f_fileP = CCFileUtils::sharedFileUtils()->fullPathForFilename("multTex_f.fsh");
    CCString *f_source = CCString::createWithContentsOfFile(f_fileP.c_str());
    const GLchar *fSource = f_source->getCString();
    
    CCGLProgram *shProg = new CCGLProgram();
    shProg->autorelease();
    if (!shProg->initWithVertexShaderByteArray(vSource, fSource)) {
        CCAssert(false, "FAILED TO COMPILE SHADER PROGRAM");
    }

    shProg->link();
    
    setShaderProgram(shProg);
    getShaderProgram()->use();
    
    _positionSlot = glGetAttribLocation(shProg->getProgram(), "Position");
    _colorSlot = glGetAttribLocation(shProg->getProgram(), "SourceColor");
    
    _texCoordSlot = glGetAttribLocation(shProg->getProgram(), "TexCoordIn");
    _textureUniform0 = glGetUniformLocation(shProg->getProgram(), "Texture0");
    _textureUniform1 = glGetUniformLocation(shProg->getProgram(), "Texture1");
    
//    _blendFactor = glGetUniformLocation(shProg->getProgram(), "BlendFactor");
    
    setupTextures();

    CCLog("pos, col slot : %d %d", _positionSlot, _colorSlot);
    CCLog("tex0 slot : %d %d", _texCoordSlot, _textureUniform0);
    CCLog("tex1 slot : %d", _textureUniform1);

    glEnableVertexAttribArray(_positionSlot);
    glEnableVertexAttribArray(_colorSlot);
    glEnableVertexAttribArray(_texCoordSlot);
    
    return true;
}

void HelloWorld::setupTextures()
{
    vector<string> fileN;
    fileN.push_back("rainbowFever.jpeg");
    fileN.push_back("terrainTexture.png");
    
    CCImage *img[m_nTextures];
    
    for (int i=0; i<m_nTextures; ++i) {
        img[i] = new CCImage;
        if (!img[i]->initWithImageFile(fileN[i].c_str())) {
            stringstream err;
            err << "failed to read image " << fileN[i].c_str();
            CCAssert(false, err.str().c_str());
        }
    }
    
    glGenTextures(m_nTextures, &texNames[0]);
    
    for (int i=0; i<m_nTextures; ++i) {
        size_t width = img[i]->getWidth();
        size_t height = img[i]->getHeight();
        
        GLubyte *sprData = img[i]->getData();
        
        glBindTexture(GL_TEXTURE_2D, texNames[i]);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, sprData);
    }
}

void HelloWorld::draw()
{
    getShaderProgram()->use();
    
    glClearColor(1.0, 0, 0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(0, 0, SCREEN_W, SCREEN_H);
    
    glVertexAttribPointer(_positionSlot, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, &vertices[0]);
    glVertexAttribPointer(_colorSlot, 4, GL_FLOAT, GL_FALSE, sizeof(float)*4, &colors[0]);
    glVertexAttribPointer(_texCoordSlot, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, &texCoords[0]);
    
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texNames[0]);
    glUniform1i(_textureUniform0, 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texNames[1]);
    glUniform1i(_textureUniform1, 1);
    
    glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]),  GL_UNSIGNED_BYTE, &Indices[0]);
}



//void HelloWorld::setupVBOs()
//{
//    GLuint vertexBuffer;
//    glGenBuffers(1, &vertexBuffer);
//    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
//
//    GLuint indexBuffer;
//    glGenBuffers(1, &indexBuffer);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
//
//    CCLog("buffers : %d %d", vertexBuffer, indexBuffer);
//}

//GLuint HelloWorld::compileShader(string shaderN, GLenum shaderType)
//{
//    string fileP = CCFileUtils::sharedFileUtils()->fullPathForFilename(shaderN.c_str());
//    CCString *fshSourceStr = CCString::createWithContentsOfFile(fileP.c_str());
//    const GLchar *fshSource = fshSourceStr->getCString();
//    const GLint fshSourceLength = fshSourceStr->length();
//
//    GLuint shaderHandle = glCreateShader(shaderType);
//    glShaderSource(shaderHandle, 1, &fshSource, &fshSourceLength);
//
//    glCompileShader(shaderHandle);
//
//    GLint compileSuccess;
//    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
//    if (compileSuccess == GL_FALSE) {
//        GLchar errorMsg[256];
//        glGetShaderInfoLog(shaderHandle, sizeof(errorMsg), 0, &errorMsg[0]);
//        CCAssert(false, errorMsg);
//    }
//
//    return shaderHandle;
//}

//void HelloWorld::compileShaders()
//{
//    GLuint vShader = compileShader("multTex_v.vsh", GL_VERTEX_SHADER);
//    GLuint fShader = compileShader("multTex_f.fsh", GL_FRAGMENT_SHADER);
//
//    GLuint programHandle = glCreateProgram();
//    glAttachShader(programHandle, vShader);
//    glAttachShader(programHandle, fShader);
//    glLinkProgram(programHandle);
//
//    GLint linkSuccess;
//    glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
//    if (linkSuccess == GL_FALSE) {
//        GLchar errorMsg[256];
//        glGetProgramInfoLog(programHandle, sizeof(errorMsg), 0, &errorMsg[0]);
//        CCAssert(false, errorMsg);
//    }
//
//    glUseProgram(programHandle);
//
//    _positionSlot = glGetAttribLocation(programHandle, "Position");
//    _colorSlot = glGetAttribLocation(programHandle, "SourceColor");
//    glEnableVertexAttribArray(_positionSlot);
//    glEnableVertexAttribArray(_colorSlot);
//
//}
