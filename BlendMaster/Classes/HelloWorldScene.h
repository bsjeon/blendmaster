#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

typedef struct {
    float Position[4];
    float Color[4];
} Vertex;

class HelloWorld : public cocos2d::CCLayerColor
{
    HelloWorld();
    virtual ~HelloWorld();
    
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    virtual void draw();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(HelloWorld);

    const float SCREEN_W = CCDirector::sharedDirector()->getWinSize().width;
    const float SCREEN_H = CCDirector::sharedDirector()->getWinSize().height;
    const int m_nTextures = 2;
    
    void setupTextures();

    GLuint m_terrainTex;
    GLuint m_highLightTex;
    GLuint _positionSlot;
    GLuint _colorSlot;

    GLuint texNames[2];
    
    GLuint _texCoordSlot;
    GLuint _textureUniform0;
    GLuint _textureUniform1;
    
    GLuint _blendFactor;
    
    
    
    void setupVBOs();
    void compileShaders();
    GLuint compileShader(string shaderN, GLenum shaderType);
    
    CCSprite *m_maskSprite;
    CCSprite *m_mySprite;
};

#endif // __HELLOWORLD_SCENE_H__
